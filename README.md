# Eurordis Contact-Database

## Installation

This project can run in a web browser only thanks to the _dist/components.js_ that is commited.
When making changes, the _dist/components.js_ must be rebuilt.
[NodeJS](https://nodejs.org/) (v12 or above) is required to install dependencies: `npm i`.
You should then copy some of these vendors: `make copy-vendors`.
Finally you can `npm run build` the _dist/components.js_ or `npm run start` a server with a watcher.

### API

This application connects to the [Contact Database API](https://gitlab.com/eurordis/contact-database) for getting some configuration and loading its data.
You should therefore install it properly and adapt the head of the _utils.js_ file with a URL that matches your setup, if you're not running on _localhost:8080_.

### Deployment

`make deploy` is a shorthand for deployment.
However it's just a link to the `eurordis deploy` task.
It fully depends on the [Contact Database Deploy](https://gitlab.com/eurordis/contact-database-deploy) project that handles all setup and commands.


## Global functions

- `request()`: a global function wrapping _fetch_ for calling APIs. Returns a JSON content when available, text otherwise.
- `apiResponse()` is an optional wrapper for `api()` that handles error statuses and others.


## Commands

- `npm start`: run the server and watch for files change. Perfect for development.
- `npm run watch`: watch for any component file change in _src/components_ and rebuild into _dist/components.js_.
- `npm run build`: build the components for production into _dist/components.js_
- `npm run copy-vendors`: copies the required assets from node modules into the vendors folder


## Dev

### Tree structure

```
dist/ # compiled files
  components.js # compiled Riot tags
src/  # source files
  components/ # Riot tags
  js/ # source JS files
    vendors/    # 3rd party JS libs
index.html  # the app entry point
```

_utils.js_ is where common code is set.

### Libs

[Riot 3](https://github.com/riot/riot/tree/v3) is used (in 2018 / 2020).
Riot 4 came out in 2019 and has changed totally, is not compatible.
Riot 3 documentation can be found [here](https://v3.riotjs.vercel.app/api/).

[PageJS](https://github.com/visionmedia/page.js) is responsible for routing.

### Gotchas

Riot internally uses the `value` attribute.
It means you may see `value_` or `_value` attributes used in order to pass a dev value into a component or on the root of the component.

`ghost mode` in the API is called `ninja mode` in this client.

### Authenticating

To access the application, authentication is required. To do so, input `contact-database@eurordis.org` in login form and access the link with the generated token printed in your local API console.

## Opiniated

- Compilation is made simple via npm command line into a single file
- Routing is based on [Page.js](https://github.com/visionmedia/page.js)
- Riot files use `.html` extension
- Caching static files is disabled on the server, no need to append the `.js?xxx` for cache refresh


## Todo

_In an ideal world_, it would be great to update the Riot Library or move to something _leaner_. [Lego](https://github.com/polight/lego) maybe?, or upgrading to Riot 4.

_organisation_ and _user_ pages (edit or details) have a lot in common. It could be cleaner to mutualize some of the code.
